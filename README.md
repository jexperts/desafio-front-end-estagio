# Desafio Front-End Estágio

Desafio prático para front-end developers (estágio) que desejam entrar para nosso time.

Objetivo: Criar um app simples de cadastro de usuários (CRUD).

## Modelo de dados:

-   Nome (obrigatório)
-   Telefone (opcional)
-   Email (obrigatório e deve ser um email válido)

## Pré-requisitos

-   Deve ser possível cadastrar, editar, listar e excluir os dados cadastrados;
-   A persistência dos dados deve ser no localStorage ou IndexedDB;
-   Obrigatório a utilização de ReactJS;
-   Utilizar a HTML 5 e CSS 3.
-   A página deve ser responsiva;
-   O projeto deve ser publicado em um repositório publico do Github/Bitbucket/Gitlab (crie uma se você não possuir).

## Desejável

-   Utilização de redux
-   Utilização de ES6 ou ES7
-   Utilização de Bootstrap 4
-   Pré-processadores Sass

## O que esperamos:

-   Utilização de boas práticas
-   Técnicas de clean code
-   Que o app suba apenas com o comando: yarn start ou npm start
